# Outils necessaires
 - Python 3.10.0 

# Setup 
(Sur windows mais c'est pratiquement pareil sur Arch/Ubuntu/Mac)

Executez dans un terminal à la racine : 
   - py -3.10 -m venv .env
   - .env\Scripts\activate
   - pip3 install -r .\requirements.txt
   - py -m rasa train



# Chat
Dans un terminal :
     
- .\.env\Scripts\activate

- py -m rasa shell

Dans une autre fenetre de terminal  :
  - .\.env\Scripts\activate
  - py -m rasa run actions



# SPECS FONCTIONNELLES

    - Réserver une table
    - Annuler une réservation
    - Afficher information réservation et modifier commentaire
    - Obtenir la liste des allergènes
    - Obtenir le menu du jour
    - Obtenir le liens vers le menu complet
    - Date de réservation, Nombre de personnes, Nom de réservation, Numéro de téléphone
    - Ajouter un commentaire à la réservation
    - Vérifier si disponible
    - Obtenir un numéro de réservation
    - Fournir l’ensemble des fichiers sur un git(hub/lab/autre), bonus si usage d’image docker ! 

  
A FAIRE :

    - Intégrer le bot sur une plateforme (pas de serv dispo)

# BUG
    - Se referer aux intents si ca ne fonctionne pas trop bien.
    


